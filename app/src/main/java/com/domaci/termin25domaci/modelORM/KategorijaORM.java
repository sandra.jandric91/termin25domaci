package com.domaci.termin25domaci.modelORM;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = KategorijaORM.TABLE_NAME_KATEGORIJA)
public class KategorijaORM {

    public static final String TABLE_NAME_KATEGORIJA = "kategorija";

    public static final String FIELD_NAME_ID = "kategorijaID";
    public static final String FIELD_NAME_KATEGORIJA = "kategorija";
    public static final String FIELD_NAME_JELO = "jelo_tabela";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private int kategorijaID;
    @DatabaseField(columnName = FIELD_NAME_KATEGORIJA)
    private String kategorija;
    @ForeignCollectionField(columnName = FIELD_NAME_JELO, eager = true)
    private ForeignCollection<JeloORM> jelaORM;

    public KategorijaORM() {
    }

    public KategorijaORM(int kategorijaID, String kategorija, ForeignCollection<JeloORM> jelaORM) {
        this.kategorijaID = kategorijaID;
        this.kategorija = kategorija;
        this.jelaORM = jelaORM;
    }

    public int getKategorijaID() {
        return kategorijaID;
    }

    public void setKategorijaID(int kategorijaID) {
        this.kategorijaID = kategorijaID;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public ForeignCollection<JeloORM> getJelaORM() {
        return jelaORM;
    }

    public void setJelaORM(ForeignCollection<JeloORM> jelaORM) {
        this.jelaORM = jelaORM;
    }

    @Override
    public String toString() {
        return kategorija;
    }
}
