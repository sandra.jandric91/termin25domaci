package com.domaci.termin25domaci.modelORM;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.CloseableWrappedIterable;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DatabaseResultsMapper;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.ObjectCache;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.dao.RawRowObjectMapper;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.GenericRowMapper;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.PreparedUpdate;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.support.DatabaseResults;
import com.j256.ormlite.table.ObjectFactory;
import com.j256.ormlite.table.TableUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    // dajemo ime bazi
    public static final String DATABASE_NAME = "ormlite.db";
    // dajemo pocetnu verziju bazi, krece od 1
    public static final int DATABASE_VERSION = 1;

    private Dao<JeloORM, Integer> jeloORMDao = new Dao<JeloORM, Integer>() {
        @Override
        public JeloORM queryForId(Integer integer) throws SQLException {
            return null;
        }

        @Override
        public JeloORM queryForFirst(PreparedQuery<JeloORM> preparedQuery) throws SQLException {
            return null;
        }

        @Override
        public List<JeloORM> queryForAll() throws SQLException {
            return null;
        }

        @Override
        public List<JeloORM> queryForEq(String fieldName, Object value) throws SQLException {
            return null;
        }

        @Override
        public List<JeloORM> queryForMatching(JeloORM matchObj) throws SQLException {
            return null;
        }

        @Override
        public List<JeloORM> queryForMatchingArgs(JeloORM matchObj) throws SQLException {
            return null;
        }

        @Override
        public List<JeloORM> queryForFieldValues(Map<String, Object> fieldValues) throws SQLException {
            return null;
        }

        @Override
        public List<JeloORM> queryForFieldValuesArgs(Map<String, Object> fieldValues) throws SQLException {
            return null;
        }

        @Override
        public JeloORM queryForSameId(JeloORM data) throws SQLException {
            return null;
        }

        @Override
        public QueryBuilder<JeloORM, Integer> queryBuilder() {
            return null;
        }

        @Override
        public UpdateBuilder<JeloORM, Integer> updateBuilder() {
            return null;
        }

        @Override
        public DeleteBuilder<JeloORM, Integer> deleteBuilder() {
            return null;
        }

        @Override
        public List<JeloORM> query(PreparedQuery<JeloORM> preparedQuery) throws SQLException {
            return null;
        }

        @Override
        public int create(JeloORM data) throws SQLException {
            return 0;
        }

        @Override
        public int create(Collection<JeloORM> datas) throws SQLException {
            return 0;
        }

        @Override
        public JeloORM createIfNotExists(JeloORM data) throws SQLException {
            return null;
        }

        @Override
        public CreateOrUpdateStatus createOrUpdate(JeloORM data) throws SQLException {
            return null;
        }

        @Override
        public int update(JeloORM data) throws SQLException {
            return 0;
        }

        @Override
        public int updateId(JeloORM data, Integer newId) throws SQLException {
            return 0;
        }

        @Override
        public int update(PreparedUpdate<JeloORM> preparedUpdate) throws SQLException {
            return 0;
        }

        @Override
        public int refresh(JeloORM data) throws SQLException {
            return 0;
        }

        @Override
        public int delete(JeloORM data) throws SQLException {
            return 0;
        }

        @Override
        public int deleteById(Integer integer) throws SQLException {
            return 0;
        }

        @Override
        public int delete(Collection<JeloORM> datas) throws SQLException {
            return 0;
        }

        @Override
        public int deleteIds(Collection<Integer> integers) throws SQLException {
            return 0;
        }

        @Override
        public int delete(PreparedDelete<JeloORM> preparedDelete) throws SQLException {
            return 0;
        }

        @Override
        public CloseableIterator<JeloORM> iterator() {
            return null;
        }

        @Override
        public CloseableIterator<JeloORM> iterator(int resultFlags) {
            return null;
        }

        @Override
        public CloseableIterator<JeloORM> iterator(PreparedQuery<JeloORM> preparedQuery) throws SQLException {
            return null;
        }

        @Override
        public CloseableIterator<JeloORM> iterator(PreparedQuery<JeloORM> preparedQuery, int resultFlags) throws SQLException {
            return null;
        }

        @Override
        public CloseableWrappedIterable<JeloORM> getWrappedIterable() {
            return null;
        }

        @Override
        public CloseableWrappedIterable<JeloORM> getWrappedIterable(PreparedQuery<JeloORM> preparedQuery) {
            return null;
        }

        @Override
        public void closeLastIterator() throws IOException {

        }

        @Override
        public GenericRawResults<String[]> queryRaw(String query, String... arguments) throws SQLException {
            return null;
        }

        @Override
        public <UO> GenericRawResults<UO> queryRaw(String query, RawRowMapper<UO> mapper, String... arguments) throws SQLException {
            return null;
        }

        @Override
        public <UO> GenericRawResults<UO> queryRaw(String query, DataType[] columnTypes, RawRowObjectMapper<UO> mapper, String... arguments) throws SQLException {
            return null;
        }

        @Override
        public GenericRawResults<Object[]> queryRaw(String query, DataType[] columnTypes, String... arguments) throws SQLException {
            return null;
        }

        @Override
        public <UO> GenericRawResults<UO> queryRaw(String query, DatabaseResultsMapper<UO> mapper, String... arguments) throws SQLException {
            return null;
        }

        @Override
        public long queryRawValue(String query, String... arguments) throws SQLException {
            return 0;
        }

        @Override
        public int executeRaw(String statement, String... arguments) throws SQLException {
            return 0;
        }

        @Override
        public int executeRawNoArgs(String statement) throws SQLException {
            return 0;
        }

        @Override
        public int updateRaw(String statement, String... arguments) throws SQLException {
            return 0;
        }

        @Override
        public <CT> CT callBatchTasks(Callable<CT> callable) throws Exception {
            return null;
        }

        @Override
        public String objectToString(JeloORM data) {
            return null;
        }

        @Override
        public boolean objectsEqual(JeloORM data1, JeloORM data2) throws SQLException {
            return false;
        }

        @Override
        public Integer extractId(JeloORM data) throws SQLException {
            return null;
        }

        @Override
        public Class<JeloORM> getDataClass() {
            return null;
        }

        @Override
        public FieldType findForeignFieldType(Class<?> clazz) {
            return null;
        }

        @Override
        public boolean isUpdatable() {
            return false;
        }

        @Override
        public boolean isTableExists() throws SQLException {
            return false;
        }

        @Override
        public long countOf() throws SQLException {
            return 0;
        }

        @Override
        public long countOf(PreparedQuery<JeloORM> preparedQuery) throws SQLException {
            return 0;
        }

        @Override
        public void assignEmptyForeignCollection(JeloORM parent, String fieldName) throws SQLException {

        }

        @Override
        public <FT> ForeignCollection<FT> getEmptyForeignCollection(String fieldName) throws SQLException {
            return null;
        }

        @Override
        public void setObjectCache(boolean enabled) throws SQLException {

        }

        @Override
        public void setObjectCache(ObjectCache objectCache) throws SQLException {

        }

        @Override
        public ObjectCache getObjectCache() {
            return null;
        }

        @Override
        public void clearObjectCache() {

        }

        @Override
        public JeloORM mapSelectStarRow(DatabaseResults results) throws SQLException {
            return null;
        }

        @Override
        public GenericRowMapper<JeloORM> getSelectStarRowMapper() throws SQLException {
            return null;
        }

        @Override
        public RawRowMapper<JeloORM> getRawRowMapper() {
            return null;
        }

        @Override
        public boolean idExists(Integer integer) throws SQLException {
            return false;
        }

        @Override
        public DatabaseConnection startThreadConnection() throws SQLException {
            return null;
        }

        @Override
        public void endThreadConnection(DatabaseConnection connection) throws SQLException {

        }

        @Override
        public void setAutoCommit(DatabaseConnection connection, boolean autoCommit) throws SQLException {

        }

        @Override
        public boolean isAutoCommit(DatabaseConnection connection) throws SQLException {
            return false;
        }

        @Override
        public void commit(DatabaseConnection connection) throws SQLException {

        }

        @Override
        public void rollBack(DatabaseConnection connection) throws SQLException {

        }

        @Override
        public ConnectionSource getConnectionSource() {
            return null;
        }

        @Override
        public void setObjectFactory(ObjectFactory<JeloORM> objectFactory) {

        }

        @Override
        public void registerObserver(DaoObserver observer) {

        }

        @Override
        public void unregisterObserver(DaoObserver observer) {

        }

        @Override
        public String getTableName() {
            return null;
        }

        @Override
        public void notifyChanges() {

        }

        @Override
        public CloseableIterator<JeloORM> closeableIterator() {
            return null;
        }
    };
    private Dao<KategorijaORM, Integer> kategorijaORMDao = null;

    // kontruktor zbog pravilne inicijalizacije baze
    public DatabaseHelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, JeloORM.class);
            TableUtils.createTable(connectionSource, KategorijaORM.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    // kad zelimo da izmenimo tabele, moramo pozvati droptable za sve tabele koje imamo
    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, JeloORM.class, true);
            TableUtils.dropTable(connectionSource, KategorijaORM.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<JeloORM, Integer> getJeloORMDao() throws SQLException {
        if (jeloORMDao == null) {
            jeloORMDao = getDao(JeloORM.class);
        }
        return jeloORMDao;
    }

    public Dao<KategorijaORM, Integer> getKategorijaORMDao() throws SQLException {
        if (kategorijaORMDao == null) {
            kategorijaORMDao = getDao(KategorijaORM.class);
        }
        return kategorijaORMDao;
    }

    @Override
    public void close() {
        jeloORMDao = null;
        kategorijaORMDao = null;
        super.close();
    }
}
