package com.domaci.termin25domaci.modelORM;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = JeloORM.TABLE_NAME_JELO)
public class JeloORM {

    public static final String TABLE_NAME_JELO = "jela";

    public static final String FIELD_NAME_ID = "id";
    public static final String FIELD_NAME_NAZIV = "naziv";
    public static final String FIELD_NAME_OPIS = "opis";
    public static final String FIELD_NAME_KATEGORIJA = "kategorija_table";
    public static final String FIELD_NAME_SASTOJCI = "sastojci";
    public static final String FIELD_NAME_KALORIJSKA_VREDNOST = "kalorijska vrednost";
    public static final String FIELD_NAME_CENA = "cena";
    public static final String FIELD_NAME_RATING = "rating";
    public static final String FIELD_NAME_IME_SLIKE = "ime slike";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = FIELD_NAME_NAZIV)
    private String naziv;
    @DatabaseField(columnName = FIELD_NAME_OPIS)
    private String opis;
    @DatabaseField(columnName = FIELD_NAME_KATEGORIJA, foreign = true, foreignAutoRefresh = true)
    private String kategorija;
    @DatabaseField(columnName = FIELD_NAME_SASTOJCI)
    private String sastojci;
    @DatabaseField(columnName = FIELD_NAME_KALORIJSKA_VREDNOST)
    private String kalorijskaVrednost;
    @DatabaseField(columnName = FIELD_NAME_CENA)
    private String cena;
    @DatabaseField(columnName = FIELD_NAME_RATING)
    private String rating;
    @DatabaseField(columnName = FIELD_NAME_IME_SLIKE)
    private String imeSlike;

    public JeloORM() { //mora prazan konstruktor za orm
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public String getSastojci() {
        return sastojci;
    }

    public void setSastojci(String sastojci) {
        this.sastojci = sastojci;
    }

    public String getKalorijskaVrednost() {
        return kalorijskaVrednost;
    }

    public void setKalorijskaVrednost(String kalorijskaVrednost) {
        this.kalorijskaVrednost = kalorijskaVrednost;
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getImeSlike() {
        return imeSlike;
    }

    public void setImeSlike(String imeSlike) {
        this.imeSlike = imeSlike;
    }

    @Override
    public String toString() {
        return naziv;
    }
}
