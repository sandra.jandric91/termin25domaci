package com.domaci.termin25domaci.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.domaci.termin25domaci.R;
import com.domaci.termin25domaci.model.Jelo;
import com.domaci.termin25domaci.modelORM.DatabaseHelper;
import com.domaci.termin25domaci.modelORM.JeloORM;
import com.j256.ormlite.android.apptools.OpenHelperManager;

public class DetailsORMActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView tvNazivJela;
    private TextView tvOpisJela;
    private TextView tvKategorija;
    private TextView tvSastojci;
    private TextView tvKalorije;
    private TextView tvCena;
    private TextView tvRating;

    private JeloORM jeloORM;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_orm);

        int jeloID = getIntent().getExtras().getInt("jeloID");

        try {
            jeloORM = getDatabaseHelper().getJeloORMDao().queryForId(jeloID);

            tvNazivJela = findViewById(R.id.tvNazivJela);
            tvOpisJela = findViewById(R.id.tvOpisJela);
            tvKategorija = findViewById(R.id.tvKategorija);
            tvSastojci = findViewById(R.id.tvSastojci);
            tvKalorije = findViewById(R.id.tvKalorije);
            tvCena = findViewById(R.id.tvCena);
            tvRating = findViewById(R.id.tvRating);

            tvNazivJela.setText(jeloORM.getNaziv());
            tvOpisJela.setText(jeloORM.getOpis());
            tvKategorija.setText(jeloORM.getKategorija());
            tvSastojci.setText(jeloORM.getSastojci());
            tvKalorije.setText(jeloORM.getKalorijskaVrednost());
            tvCena.setText(jeloORM.getCena());
            tvRating.setText(jeloORM.getRating());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


}
