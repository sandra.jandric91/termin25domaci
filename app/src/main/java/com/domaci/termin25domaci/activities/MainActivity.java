package com.domaci.termin25domaci.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.domaci.termin25domaci.AboutDijalog;
import com.domaci.termin25domaci.App;
import com.domaci.termin25domaci.R;
import com.domaci.termin25domaci.fragmenti.DetailsFragment;
import com.domaci.termin25domaci.fragmenti.KategorijaFragment;
import com.domaci.termin25domaci.fragmenti.ListFileFragment;
import com.domaci.termin25domaci.fragmenti.ListFragmentRV;
import com.domaci.termin25domaci.fragmenti.ListORMFragment;
import com.domaci.termin25domaci.fragmenti.PrefsFragment;
import com.domaci.termin25domaci.model.Jelo;
import com.domaci.termin25domaci.modelORM.DatabaseHelper;
import com.domaci.termin25domaci.modelORM.JeloORM;
import com.domaci.termin25domaci.modelORM.KategorijaORM;
import com.domaci.termin25domaci.servis22.SimpleService;
import com.domaci.termin25domaci.tools.ReviewerTools;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ListORMFragment.OnJeloORMSelectedListener {

    private boolean landscapeMode = false;
    private Toolbar toolbar;

    private List<String> drawerItems;
    private LinearLayout linearZaDrawerList;
    private DrawerLayout drawerLayout;
    private ListView drawer_list;
    private ActionBarDrawerToggle drawerToggle;

    private AlertDialog dijalog;
    public static final int NOTIF_ID = 22;
    private ProgressDialog progressDialog;
    public static final String TAG = "PERMISSIONS";

    private DatabaseHelper databaseHelper;

    private EditText etNazivJela;
    private EditText etOpisJela;
    private EditText etSastojci;
    private EditText etKalorije;
    private EditText etCena;
    private EditText etRating;
    private Spinner sKategorija;
    private Spinner sSlika;
    private Button bOK;
    private Button bCancel;

    private List<JeloORM> jelaORM;
    private ListView listZaJelaORM;
    private ArrayAdapter<JeloORM> adapterJeloORM;

    private int jeloID;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /* if (savedInstanceState == null) {
            showRecyclerList();
        }
        showDetails(); */

        setupToolbar();
        fillDrawerWithItems();
        setupDrawer();
        showList();
    }

    private void showList() {
        if (landscapeMode) {
            ListORMFragment fragment = new ListORMFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.linear_za_master, fragment);
            transaction.commit();
        } else {
            ListORMFragment fragment = new ListORMFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.master_view, fragment);
            transaction.commit();
        }
    }

    private void showRecyclerList() {
        new MyAsyncTask().execute(1);
    }

    @Override
    public void onJeloORMSelected(int id) {
        jeloID = id;
        try {
            JeloORM jeloORM = getDatabaseHelper().getJeloORMDao().queryForId(id);
            Intent intent = new Intent(this, DetailsORMActivity.class);
            intent.putExtra("jeloID", jeloID);
            startActivity(intent);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private class MyAsyncTask extends AsyncTask<Integer, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Download liste u toku");
            progressDialog.setMessage("Molimo sačekajte...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Integer... params) {
            for (int i = params[0]; i >= 0; i--) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            ListFragmentRV fragment = new ListFragmentRV();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.master_view, fragment);
            transaction.commit();
        }
    }

  //  @Override
    public void onJeloClicked(int id) {
        if (landscapeMode) {
            DetailsFragment fragment = new DetailsFragment();
            fragment.setJeloID(id);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.detail_view, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else {
            DetailsFragment fragment = new DetailsFragment();
            fragment.setJeloID(id);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.master_view, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    private void showDetails() {
        if (findViewById(R.id.detail_view) != null) {
            landscapeMode = true;
            getSupportFragmentManager().popBackStack();

            DetailsFragment fragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.detail_view);
            if (fragment != null) {
                fragment = new DetailsFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.detail_view, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        }
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_home);
            actionBar.show();
        }
    }

    private void fillDrawerWithItems() {
        drawerItems = new ArrayList<>();
        drawerItems.add("Kategorija");
        drawerItems.add("Jelo");
        drawerItems.add("Podesavanja");
        drawerItems.add("About");
        drawerItems.add("Notifikacija");
    }

    private void setupDrawer() {
        linearZaDrawerList = findViewById(R.id.linear_za_drawerList);
        drawerLayout = findViewById(R.id.drawerLayout);
        drawer_list = findViewById(R.id.drawer_list);
        drawer_list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, drawerItems));
        drawer_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String naslov = "Nepoznato";
                switch (position) {
                    case 0:
                        naslov = "Kategorija";
                        showKategorija();
                        break;
                    case 1:
                        naslov = "Jelo";
                        showRecyclerList();
                        break;
                    case 2:
                        naslov = "Podešavanja";
                        showSettings();
                        break;
                    case 3:
                        naslov = "About";
                        showAbout();
                        break;
                    case 4:
                        naslov = "Notifikacija";
                        showNotifikacija();
                }
                setTitle(naslov);
                drawerLayout.closeDrawer(linearZaDrawerList);
            }
        });

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View view) {
                invalidateOptionsMenu();
            }
        };
        drawerLayout.closeDrawer(linearZaDrawerList);
    }

    private void showKategorija() {
        KategorijaFragment fragment = new KategorijaFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (landscapeMode) {
            transaction.replace(R.id.linear_za_master, fragment);
        } else {
            transaction.replace(R.id.master_view, fragment);
        }
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showSettings() {
        PrefsFragment fragment = new PrefsFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (landscapeMode) {
            transaction.replace(R.id.linear_za_master, fragment);
        } else {
            transaction.replace(R.id.master_view, fragment);
        }
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showAbout() {
        if (dijalog == null) {
            dijalog = new AboutDijalog(this).prepareDialog();
        } else {
            if (dijalog.isShowing()) {
                dijalog.dismiss();
            }
        }
        dijalog.show();
    }

    private void showNotifikacija() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), App.CHANNEL_ID);
        builder.setContentTitle("Notifikacija")
                .setContentText("Stigla vam je notifikacija")
                .setSmallIcon(R.drawable.ic_notif);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIF_ID, builder.build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_connection:
                Intent intent = new Intent(MainActivity.this, SimpleService.class);
                intent.putExtra(SimpleService.CONNECTION_TYPE, 0);
                startService(intent);
                break;
            case R.id.action_create:
                Intent intent1 = new Intent(MainActivity.this, KomentarActivity.class);
                startActivity(intent1);
                break;
            case R.id.action_edit:
                if (isStoragePermissionGranted()) {
                    boolean exists = ReviewerTools.isFileExists(this, "myfile.txt");
                    if (exists) {
                        Toast.makeText(this, "Fajl vec postoji u bazi", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Fajl ne postoji u bazi", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.action_delete:
                Toast.makeText(this, "Uspesno obrisano", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_check:
                if (isStoragePermissionGranted()) {
                    String text = ReviewerTools.readFromFile(this, "myfile.txt");
                    Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.action_add_file:
                if (isStoragePermissionGranted()) {
                    ReviewerTools.writeToFile(new Date().toString(), this, "myfile.txt");
                }
                break;
            case R.id.action_read:
                if (isStoragePermissionGranted()) {
                    ListFileFragment fragment = new ListFileFragment();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    if (landscapeMode) {
                        transaction.replace(R.id.linear_za_master, fragment);
                    } else {
                        transaction.replace(R.id.master_view, fragment);
                    }
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                break;
            case R.id.action_create_jelo:
                createJelo();
                break;
            case R.id.action_refresh:
                refresh();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + " was " + grantResults[0]);
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onResume() {
        refresh();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    private void createJelo() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.create_jelo_fragment);
        dialog.setCancelable(false);

        etNazivJela = dialog.findViewById(R.id.etNazivJela);
        etOpisJela = dialog.findViewById(R.id.etOpisJela);
        etSastojci = dialog.findViewById(R.id.etSastojci);
        etKalorije = dialog.findViewById(R.id.etKalorije);
        etCena = dialog.findViewById(R.id.etCena);
        etRating = dialog.findViewById(R.id.etRating);
        bOK = dialog.findViewById(R.id.bOK);
        bCancel = dialog.findViewById(R.id.bCancel);

        sKategorija = dialog.findViewById(R.id.sKategorija);
        ArrayAdapter<String> adapterKategorija = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.kategorija_naziv));
        sKategorija.setAdapter(adapterKategorija);
        String [] data = getResources().getStringArray(R.array.kategorija_naziv);
        for (int i = 0; i < data.length; i++) {
            if (data[i].equalsIgnoreCase(KategorijaORM.FIELD_NAME_KATEGORIJA)) {
                sKategorija.setSelection(i);
            }
        }

        sSlika = dialog.findViewById(R.id.sSlika);
        ArrayAdapter<String> adapterSlika = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.slike_array));
        sSlika.setAdapter(adapterSlika);
        String [] dataSlika = getResources().getStringArray(R.array.slike_array);
        for (int i = 0; i < dataSlika.length; i++) {
            if (dataSlika[i].equalsIgnoreCase(JeloORM.FIELD_NAME_IME_SLIKE)){
                sSlika.setSelection(i);
            }
        }

        bOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String naziv = etNazivJela.getText().toString();
                String opis = etOpisJela.getText().toString();
                String sastojci = etSastojci.getText().toString();
                String kalorije = etKalorije.getText().toString();
                String cena = etCena.getText().toString();
                String rating = etRating.getText().toString();
                String kategorija = sKategorija.getSelectedItem().toString();
                String slika = sSlika.getSelectedItem().toString();

                JeloORM jelo = new JeloORM();
                jelo.setNaziv(naziv);
                jelo.setOpis(opis);
                jelo.setSastojci(sastojci);
                jelo.setKalorijskaVrednost(kalorije);
                jelo.setCena(cena);
                jelo.setRating(rating);
                jelo.setKategorija(kategorija);
                jelo.setImeSlike(slika);

                try {
                    getDatabaseHelper().getJeloORMDao().create(jelo);
                    refresh();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void refresh() {

        if (listZaJelaORM != null) {
            adapterJeloORM = (ArrayAdapter<JeloORM>) listZaJelaORM.getAdapter();
            if (adapterJeloORM != null) {
                try {
                    adapterJeloORM.clear();
                    jelaORM = getDatabaseHelper().getJeloORMDao().queryForAll();
                    adapterJeloORM.addAll(jelaORM);
                    adapterJeloORM.notifyDataSetChanged();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
