package com.domaci.termin25domaci.servis23;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.domaci.termin25domaci.App;
import com.domaci.termin25domaci.R;
import com.domaci.termin25domaci.activities.KomentarActivity;
import com.domaci.termin25domaci.tools.ReviewerTools;

public class SimpleReceiver extends BroadcastReceiver {

    private static final int NOTIFICATION_ID = 1;
    private static final int NOTIFICATION_ID2 = 2;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("KOMENTAR")) {
            String komentarZaNotif = intent.getExtras().getString("KOMENTAR_ZA_NOTIF");
            if (KomentarActivity.dozvoliNotifikacije == true) {
                prepareNotification(komentarZaNotif, context);
            } else {
                Toast.makeText(context, "Notifikacije nisu dozvoljene", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void prepareNotification(String komentarZaNotif, Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, App.CHANNEL_ID);
        int connectionType = ReviewerTools.getConnectivityStatus(context);
        if (connectionType == ReviewerTools.TYPE_WIFI) {
            String text = komentarZaNotif;
            builder.setSmallIcon(R.drawable.ic_wifi_connection);
            builder.setContentTitle("Komentar (WIFI):");
            builder.setContentText(text);
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        }
        if (connectionType == ReviewerTools.TYPE_MOBILE) {
            String text = komentarZaNotif;
            builder.setSmallIcon(R.drawable.ic_mobile_connection);
            builder.setContentTitle("Komentar (DATA):");
            builder.setContentText(text);
            notificationManager.notify(NOTIFICATION_ID2, builder.build());
        }
    }
}
