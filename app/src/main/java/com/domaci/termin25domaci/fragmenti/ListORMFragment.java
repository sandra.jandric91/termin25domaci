package com.domaci.termin25domaci.fragmenti;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.domaci.termin25domaci.R;
import com.domaci.termin25domaci.modelORM.DatabaseHelper;
import com.domaci.termin25domaci.modelORM.JeloORM;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ListORMFragment extends Fragment {

    private DatabaseHelper databaseHelper;
    private OnJeloORMSelectedListener listener;
    private ListAdapter adapter;
    private List<JeloORM> jeloORM;
    private ListView listView;

    public interface OnJeloORMSelectedListener{
        void onJeloORMSelected (int id);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.list_orm_fragment, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            jeloORM = getDatabaseHelper().getJeloORMDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (jeloORM == null) {
            jeloORM = new ArrayList<>();
        }

        listView = getView().findViewById(R.id.lista_u_ormfragment);
        adapter = new ArrayAdapter<JeloORM>(getActivity(), android.R.layout.simple_list_item_1, jeloORM);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JeloORM jeloORM = (JeloORM) listView.getAdapter();
                listener.onJeloORMSelected(jeloORM.getId());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (OnJeloORMSelectedListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return databaseHelper;
    }
}
